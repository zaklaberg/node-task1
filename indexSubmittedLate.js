const http = require('http');
const fs = require('fs');
const os = require('os');
const readline = require('readline');

class Well {
    constructor() {
        this.options = [];
        this.optionsText = 'Choose an option:\n';

        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
    }

    addOption(descriptor, valueToSelectBy, action) {
        this.options.push({valueToSelectBy, action})
        this.optionsText += `${valueToSelectBy}. ` + descriptor + '\n';
    }

    askUserForOption() {
        this.rl.question(this.optionsText + 'Type a number: ', choice => {
            choice = parseInt(choice, 10);
            const validChoices = this.options.map(option => option.valueToSelectBy);
            if (validChoices.includes(choice)) {
                this.options.find(obj => obj.valueToSelectBy === choice).handler.call(this);
            }
            else {
                this.rl.write('Invalid option.');
                this.rl.close();
            }
        });
    }
}

function readPackageJSON() {
    fs.readFile('./package.json', 'utf-8', (err, data) => {
        if (err) this.rl.write(err);
        else this.rl.write(data);
        this.rl.close();
    });
}

function displaySystemInfo() {
    const totalmem = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const freemem = (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const cpucores = os.cpus().length;
    const arch = os.arch();
    const platform = os.platform();
    const release = os.release();
    const user = os.userInfo().username;
    
    let infoOut = `SYSTEM MEMORY: ${totalmem}\n`;
    infoOut += `FREE MEMORY: ${freemem}\n`;
    infoOut += `CPU CORES: ${cpucores}\n`;
    infoOut += `ARCHITECTURE: ${arch}\n`;
    infoOut += `PLATFORM: ${platform}\n`;
    infoOut += `RELEASE: ${release}\n`;
    infoOut += `USER: ${user}`;

    this.rl.write(infoOut);
    this.rl.close();
}

function startHTTPServer() {
    console.log('Starting HTTP server...');
    const server = http.createServer((req, res) => {

    });

    console.log('Listening on port 3000...');
    server.listen(3000);
    this.rl.close();
}

const wellApp = new Well();
wellApp.addOption('Read package.json', 1, readPackageJSON);
wellApp.addOption('Display system info', 2, displaySystemInfo);
wellApp.addOption('Start HTTP server', 3, startHTTPServer);

wellApp.askUserForOption();

