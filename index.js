const http = require('http');
const fs = require('fs');
const os = require('os');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function readPackageJSON() {
    fs.readFile('./package.json', 'utf-8', (err, data) => {
        if (err) rl.write(err);
        else rl.write(data);
        rl.close();
    });
}

function displayOSInfo() {
    
    const totalmem = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const freemem = (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const cpucores = os.cpus().length;
    const arch = os.arch();
    const platform = os.platform();
    const release = os.release();
    const user = os.userInfo().username;
    
    const infoOut = `SYSTEM MEMORY: ${totalmem}\nFREE MEMORY: ${freemem}\nCPU CORES: ${cpucores}\nARCHITECTURE: ${arch}\nPLATFORM: ${platform}\nRELEASE: ${release}\nUSER: ${user}`;
    rl.write(infoOut);
    rl.close();
}

function startHTTPServer() {
    console.log('Starting HTTP server...');
    const server = http.createServer((req, res) => {

    });

    console.log('Listening on port 3000...');
    server.listen(3000);
    rl.close();
}

const question = `Choose an option:\n1: Read package.json\n2. Display OS info\n3.Start HTTP server\nType a number: `;
rl.question(question, choice => {
    choice = parseInt(choice, 10);
    switch(choice) {
        case 1:
            readPackageJSON();
            break;
        case 2: 
            displayOSInfo();
            break;
        case 3:
            startHTTPServer();
            break;
        default:
            rl.write("Invalid option.");
            rl.close();
    }
});




